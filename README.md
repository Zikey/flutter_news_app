# flutter_news_app
Getting news from https://newsapi.org/ and show as list.
Open each news in webView.
Bookmark each news in local db.




## important libraries

- Hive : database.
- Dio : Restfull api.
- Bloc : State managment.
- GetIt : Dependency injection.
- Mockito : Testing.
- Bloc_Test: Testing 

## Test
- [TDD](https://en.wikipedia.org/wiki/Test-driven_development) 


## Clean Architecture (Must)

<img src="./clean_architecture.png" max-width="250px" max-height="250px" />


