import 'package:dio/dio.dart';
import 'package:flutter_news_app/core/constants/constants.dart';

class ApiProvider{

  final Dio _dio = Dio();
  var apiKey = Constants.apiKey;

  Future<dynamic> callNews(country) async{

    var response = await _dio.get(
        '${Constants.api}/top-headlines',
        queryParameters: {
          'country' : country,
          'apiKey' : apiKey,
        }
    );
    return response;
  }

}
