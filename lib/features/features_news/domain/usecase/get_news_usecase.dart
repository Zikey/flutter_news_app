import 'package:flutter_news_app/core/resource/data_state.dart';
import 'package:flutter_news_app/core/usecase/usecase.dart';
import 'package:flutter_news_app/features/features_news/domain/entities/news_entitiy.dart';
import 'package:flutter_news_app/features/features_news/domain/repository/news_repository.dart';

class GetNewsUseCase extends UseCase<DataState<List<NewsEntity>>, String> {
  final NewsRepository newsRepository;

  GetNewsUseCase(this.newsRepository);

  @override
  Future<DataState<List<NewsEntity>>> call(String p) {
    return newsRepository.getNewsFromApi(p);
  }
}
