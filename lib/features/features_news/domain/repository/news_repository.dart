
import 'package:flutter_news_app/core/resource/data_state.dart';
import 'package:flutter_news_app/features/features_news/data/model/news.dart';
import 'package:flutter_news_app/features/features_news/domain/entities/news_entitiy.dart';

abstract class NewsRepository{

  Future<DataState<List<NewsEntity>>> getNewsFromApi(String language);

}