import 'package:equatable/equatable.dart';
import 'package:flutter_news_app/features/features_news/data/model/news.dart';



class NewsEntity extends Equatable{

 final String? status;
 final int? totalResults;
 final List<Articles>? articles;

  const NewsEntity({this.status, this.totalResults, this.articles});

  @override
  // TODO: implement props
  List<Object?> get props =>[];


}