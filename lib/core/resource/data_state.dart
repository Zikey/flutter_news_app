abstract class DataState<Type>{
  final Type? data;
  final String? error;

  DataState(this.data, this.error);
}

class DataSuccess<Type> extends DataState<Type>{
  DataSuccess(Type? data):super(data,null);

}

class DataFailed<Type> extends DataState<Type>{
  DataFailed(String error):super(null,error);
}