import 'package:flutter/material.dart';
import 'package:flutter_news_app/features/features_news/presentation/pages/news_screen.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const NewsScreen());
}


