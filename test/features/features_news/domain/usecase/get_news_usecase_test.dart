
import 'package:flutter_news_app/core/constants/constants.dart';
import 'package:flutter_news_app/core/resource/data_state.dart';
import 'package:flutter_news_app/features/features_news/domain/entities/news_entitiy.dart';
import 'package:flutter_news_app/features/features_news/domain/repository/news_repository.dart';
import 'package:flutter_news_app/features/features_news/domain/usecase/get_news_usecase.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'get_news_usecase_test.mocks.dart';

@GenerateMocks([NewsRepository])
void main() {
  late MockNewsRepository mockNewsRepository;
  late GetNewsUseCase getNewsUseCase;

  setUp(() {
    mockNewsRepository = MockNewsRepository();
    getNewsUseCase = GetNewsUseCase(mockNewsRepository);
  });

  test('get news usecase test', () async {
    final List<NewsEntity> news = <NewsEntity>[];
    final dataSuccess=DataSuccess(news);
    final response = Future.value(dataSuccess);

    when(mockNewsRepository.getNewsFromApi(any))
        .thenAnswer((_) async => response);

    final result = await getNewsUseCase(Language.english);
    expect(result, dataSuccess);
  });
}
